import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FormGroup, Input, Label, Button } from 'reactstrap';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../components/forms/forms.css'
const OverallView = ({ setOverallViewData, higherEduData }) => {

    const [userID, setUserID] = useState("")
    const [userName, setUserName] = useState("")
    const [userPresent, setuserPresent] = useState(false)
    const [userData, setUserData] = useState({})
    const [errors, seterror] = useState({
        userID:"",
        userName:""
    })

    // useEffect(() => {
    //     console.log("userID :- ", userID)
    //     console.log("userPresent :- ", userPresent)
    //     console.log("userName :- ", userName)
    //     console.log("userData :- ", userData)
    // }, [userID, userPresent, userName, userData])
    const validate = (name, value) => {
        switch (name) {
            case "ID of applicant":
                let regExp = /^API/;
              if (!value || value.trim() === "") {
                seterror({...errors, userID:"Id is Required"})
              } 
              else if( !regExp.test( value ) ) {
                seterror({...errors, userID:"Id should start with API"})
                }
              else {
                seterror({...errors, userID:""})
              }
            break;
            default: {
              return "";
            }
          }
    }
    useEffect(() => {
        Object.keys(userData).length === 0 ? setuserPresent(false) : setuserPresent(true)
    }, [userData])

    useEffect(() => {
        setOverallViewData({
            isIdPresent: userPresent,
            userID: userID,
            userName: userName,
        })
    }, [userID, userPresent, userName, setOverallViewData])

    const getUsersData = async () => {
        axios
            .get(`http://localhost:3001/colorcode/user/${userID}`)
            .then((Response) => {
                if (Response.data !== null && Array.isArray(Response.data) === false) {
                    setUserData(Response.data)
                    setUserName(Response.data.userName)
                    // sendToast()
                }
            }).catch((Error) => {
                console.log(Error)
            })
    }

    // const sendToast = () => {
    //     Object.keys(userData).length === 0 && userPresent === false ? toast.error("User not Found") : toast.success("User Found and Data Loaded Sucessfully")
    // }

    const onUserIdChange = (event) => {
        setUserID(event.target.value)
    }
    const onUserNameChange = (event) => {
        setUserName(event.target.value)
    }

    return (
        <div>
            <div className="row pb-4">
                <div className="col-sm-12 col-md-6 col-lg-4">
                    <FormGroup floating>
                        <Input
                            id="ID of applicant"
                            name="ID of applicant"
                            placeholder="ID of applicant"
                            type="text"
                            onChange={(e) => {validate(e.target.name, e.target.value);onUserIdChange(e)}}
                        />
                        <Label for="ID of applicant">
                            ID of applicant
                        </Label>
                        <div className='text-danger'>{errors.userID}</div>
                    </FormGroup>
                </div>
                {Object.keys(userData).length !== 0 && userPresent === true ?
                    <div className="col-sm-12 col-md-6 col-lg-4">
                        <FormGroup floating>
                            <Input
                                id="Name of applicant"
                                name="Name of applicant"
                                placeholder="Name of applicant"
                                type="text"
                                value={userData.userName}
                                disabled
                            />
                            <Label for="ID of applicant">
                                Name of applicant
                            </Label>
                        </FormGroup>
                    </div>
                    :
                    <div className="col-sm-12 col-md-6 col-lg-4">
                        <FormGroup floating>
                            <Input
                                id="Name of applicant"
                                name="Name of applicant"
                                placeholder="Name of applicant"
                                type="text"
                                onChange={(event) => onUserNameChange(event)}
                            />
                            <Label for="ID of applicant">
                                Name of applicant
                            </Label>
                        </FormGroup>
                    </div>
                }

                <div className="col-sm-12 col-md-6 col-lg-4" >
                    <FormGroup floating >
                        <Input
                            id="OverallColorCode"
                            name="OverallColorCode"
                            placeholder="OverallColorCode"
                            type="text"
                            disabled
                        />
                                <Label for="OverallColorCode">
                                </Label>
                        
                    </FormGroup>
                </div>
                <div className="col pb-4">
                    <center>
                        {userPresent === true ?
                            <Button
                                color="primary"
                                onClick={() => getUsersData()}
                                className='btn-md me-2'
                                disabled
                            >Get User</Button>
                            :
                            <Button
                                color="primary"
                                onClick={() => getUsersData()}
                                className='btn-md me-2'
                            >Get User</Button>
                        }

                        <Button
                            color="primary"
                            className='btn-md ms-2'
                            onClick={() => window.location.reload()}
                        >
                            Reset Forms
                        </Button>
                    </center>
                </div>
            </div>
        </div >
    )
}

export default OverallView