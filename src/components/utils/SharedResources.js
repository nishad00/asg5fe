const colorCodesForDropdown = [
    {
        name:'Red',
        value:'Red',
        style:{backgroundColor:"#ff0000", color:"white"}
    },{
        name:'Orange',
        value:'Orange',
        style:{backgroundColor:"#ff6600", color:"white"}
    },{
        name:'Green',
        value:'Green',
        // style:{backgroundColor:"#339933"}
        style:{backgroundColor:"#0BDA51", color:"white"}

    },{
        name:'Yellow',
        value:'Yellow',
        // style:{backgroundColor:"#339933"}
        style:{backgroundColor:"yellow", color:"black"}

    }
];

export {
    colorCodesForDropdown
}
