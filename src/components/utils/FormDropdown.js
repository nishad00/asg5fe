import React, { useState } from "react";
import {Form} from 'react-bootstrap';

const FormDropdown = (props) => {
    const [drState, setDrS] = useState({})
    const onValueChange = (e) => {
        setDrS({style: props.options[e.target.selectedIndex-1]['style']})
        props.onChange(e);
    }
    return (
        <Form.Select aria-label={"Hello"} onChange={(e) => {onValueChange(e)}} style={drState.style}>
            <option>Select Color</option>
            {props.options.map((opt) => {return <option value={opt.value} style={('style' in opt) ?opt.style : {}}>{opt.name}</option>})}
        </Form.Select>
    );
}
export default FormDropdown;