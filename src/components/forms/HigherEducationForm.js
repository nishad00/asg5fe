import React, { useEffect, useState } from "react";
import {Container, Row, Col,Form, Button} from 'react-bootstrap';
import { colorCodesForDropdown } from "../utils/SharedResources";
import FormDropdown from "../utils/FormDropdown";
import '../components.css'
import axios from "axios";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './forms.css'
// import { Formik, Form, Field } from "formik";

const HigherEducationForm =  ({ overallViewData, setHigherEduData }) => {

    const [userData, setUserData] = useState({
        userId : "",
        userName : ""
    })
    
    const [formState, setFormState] = useState({
        user : {},
        degree: "",
        uniName: "",
        uniAddress: "",
        rollNo: "",
        cgpa: "",
        year: "",
        remarks: "",
        colorCode: "",
    }); 
    const [errors, seterror] = useState({...formState});
    const [isCorrect, setCorrect] = useState(true);
    useEffect(() => {
        setUserData({
            userId : overallViewData.userID,
            userName : overallViewData.userName,
        })
        setFormState({
            ...formState,
            user : userData,
        });
        setHigherEduData({
            remarks : formState.remarks,
            color : formState.colorCode,
        })

    },[overallViewData,formState]);
    
    const validate = (name, value) => {
        switch (name) {
            case "degree":
              if (!value || value.trim() === "") {
                seterror({...errors, degree:"Degree is Required"})
              } else {
                seterror({...errors, degree:""})
              }
            break;
            case "uniName":{
                if (!value || value.trim() === "") {
                    seterror({...errors, uniName:"University Name is Required"})
                  } else {
                    seterror({...errors, uniName:""})
                  }
            }
            break;
            case "uniAddress":{
                let regExp = /^[a-zA-Z]*$/;
                if (!value || value.trim() === "") {
                    seterror({...errors, uniAddress:"City is Required"})
                  }
                  else if( !regExp.test( value ) ) {
                    seterror({...errors, uniAddress:"Enter Valid City Name"})
                    }
                  
                   else {
                    seterror({...errors, uniAddress:""})
                  }
            }
            break;
            case "year":{
                if (!value || value === "") {
                    seterror({...errors, year:"Passing Year is Required"})
                  } 
                  else if(value<0){
                    seterror({...errors, year:"Passing Year is should be Positive"})
                  }
                  else {
                    seterror({...errors, year:""})
                  }
            }
            break;
            case "cgpa":{
                if (!value || value.trim() === "") {
                    seterror({...errors, cgpa:"CGPA is Required"})
                  }
                  else if(value<0){
                    seterror({...errors, cgpa:"CGPA is should be Positive"})
                  }
                  else {
                    seterror({...errors, cgpa:""})
                  }
            }
            break;
            case "remarks":{
                if (!value || value.trim() === "") {
                    seterror({...errors, remarks:"Remark is Required"})
                  } else {
                    seterror({...errors, remarks:""})
                  }
            }
            break;
            default: {
              return "";
            }
          }
    }
    const onFormSubmit = () =>
    {
       
        // axios.post("http://localhost:3001/colorcode/education",formState)
        // .then((Response) => {
        //     if (Response.status === 200 ) {
        //         toast.success("Data Added Sucessfully");
        //     }
        // }).catch((error) => {
        //     console.log(error)
        // })
     
    }
    
    return (
        <div className="education-form">
            <h2><u>Professional Education</u></h2>
            <Container className="edu-main-form">
            <Row>
                <Col lg={6} md={6} sm={12}>     
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Name of applicant</Form.Label>
                        <Form.Control type="text" placeholder="Enter Applicant Name" value={overallViewData.userName} disabled />
                    </Form.Group>
                </Col>
                <Col lg={6} md={6} sm={12}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Degree</Form.Label>
                        <Form.Control type="text" name="degree" placeholder="Enter Degree" onChange={e => {validate('degree',e.target.value);setFormState({...formState, degree:e.target.value})}} />
                        <div className="text-danger">{errors.degree}</div>
                    </Form.Group>
                </Col>
            </Row>
            <Row>
            <Col lg={6} md={6} sm={12}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Name of University</Form.Label>
                        <Form.Control type="text" placeholder="Enter University Name" name="uniName" onChange={e => {validate('uniName',e.target.value);setFormState({...formState, uniName:e.target.value})}} />
                        <div className="text-danger">{errors.uniName}</div>
                    </Form.Group>
                </Col>
                <Col lg={6} md={6} sm={12}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>City</Form.Label>
                        <Form.Control type="text"  name="uniAddress" onChange={e => {validate('uniAddress',e.target.value);setFormState({...formState, uniAddress:e.target.value})}} />
                        <div className="text-danger">{errors.uniAddress}</div>
                    </Form.Group>
                </Col>
                
            </Row>
            <Row>
                <Col lg={6} md={6} sm={12}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Passing Year</Form.Label>
                        <Form.Control type="number" placeholder="Enter Passing Year" onChange={e => {validate('year',e.target.value);setFormState({...formState, year:e.target.value})}} />
                        <div className="text-danger">{errors.year}</div>
                    </Form.Group>
                </Col>
                <Col lg={6} md={6} sm={12}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>CGPA</Form.Label>
                        <Form.Control type="number" placeholder="Enter CGPA" onChange={e => {validate('cgpa',e.target.value);setFormState({...formState, cgpa:e.target.value})}} />
                        <div className="text-danger">{errors.cgpa}</div>
                    </Form.Group>
                </Col>
            </Row>
            <Row>
                <Col lg={6} md={12} sm={12}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Summary/Remarks</Form.Label>
                        <Form.Control type="text" onChange={e => {validate('remarks',e.target.value);setFormState({...formState, remarks:e.target.value})}} />
                        <div className="text-danger">{errors.remarks}</div>
                    </Form.Group>
                </Col>
                <Col lg={6} md={6} sm={12}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Color Code</Form.Label>
                        <FormDropdown options={colorCodesForDropdown} title="Select Color Code" onChange={e => setFormState({...formState, colorCode:e.target.value})}/>
                    </Form.Group>
                </Col>
            </Row>
            <Button variant="primary" onClick={onFormSubmit}>
                Submit
            </Button>
            </Container>
        </div>
    );
}
export default HigherEducationForm;