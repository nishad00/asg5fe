import React, { useEffect, useState } from "react";
import {Container, Row, Col, Form, Button} from 'react-bootstrap';
import { colorCodesForDropdown } from "../utils/SharedResources";
import FormDropdown from "../utils/FormDropdown";
import '../components.css'
import axios from "axios";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const CriminalForm =  ({ overallViewData, setCriminalData }) => {

    const [userData, setUserData] = useState({
        userId : "",
        userName : ""
    })

    const [formState, setFormState] = useState({
        user : {},
        crimehistory: "",
        remarks: "",
        colorCode: "",
    }); 

    useEffect(() => {
        setUserData({
            userId : overallViewData.userID,
            userName : overallViewData.userName,
        })
        setFormState({
            ...formState,
            user : userData,
        });
        setCriminalData({
            remarks : formState.remarks,
            color : formState.colorCode,
        })

    },[overallViewData,formState])

    const onFormSubmit = () =>
    {
        console.log("I am Criminal", formState);
        axios.post("http://localhost:3001/colorcode/education",formState)
        .then((Response) => {
            if (Response.status === 200 ) {
                toast.success("Data Added Sucessfully");
            }
        }).catch((error) => {
            console.log(error)
        })
     
    }
    
    return (
        <Form className="education-form">
            <h2><u>Criminal</u></h2>
            <Container className="edu-main-form">
            <Row>
                <Col lg={6} md={6} sm={12}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Name of applicant</Form.Label>
                        <Form.Control type="text" placeholder="Enter Applicant Name" value={overallViewData.userName} disabled />
                    </Form.Group>
                </Col>
                <Col lg={6} md={6} sm={12}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Criminal History</Form.Label>
                        <Form.Control type="text" placeholder="Enter criminal history" onChange={e => setFormState({...formState, crimehistory:e.target.value})} />
                    </Form.Group>
                </Col>
            </Row>
            <Row>
                <Col lg={6} md={12} sm={12}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Summary/Remarks</Form.Label>
                        <Form.Control type="text" onChange={e => setFormState({...formState, remarks:e.target.value})} />
                    </Form.Group>
                </Col>
                <Col lg={6} md={6} sm={12}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Color Code</Form.Label>
                        <FormDropdown options={colorCodesForDropdown} title="Select Color Code" onChange={e => setFormState({...formState, colorCode:e.target.value})} />
                    </Form.Group>
                </Col>
            </Row>
            <Button variant="primary" onClick={onFormSubmit} >
                Submit
            </Button>
            </Container>
        </Form>
    );
}
export default CriminalForm