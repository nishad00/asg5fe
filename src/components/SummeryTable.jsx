import React from 'react'
import { Table } from 'reactstrap'
import "../style/summery.css"

const SummeryTable = ({ higherEduData, IdentityData, CriminalData, PreviousEmploymentData, ProjectDetailsData }) => {
    return (
        <div>
            <div className="row">
                <div className="col">
                    <Table bordered responsive >
                        <thead>
                            <tr >
                                <th colSpan={2}>
                                    Components
                                </th>
                                <th>
                                    Review
                                </th>
                                <th>
                                    Status
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr >
                                <td className='custom_td'>
                                    Education
                                </td>
                                <td>
                                    Professional Education
                                </td>
                                <td>
                                    {higherEduData.remarks}
                                </td>
                                <td style={{ background: higherEduData.color, color: "white" }}>
                                    {higherEduData.color}
                                </td>
                            </tr>
                            <tr>
                                <td className='custom_td'>
                                    Identity
                                </td>
                                <td>
                                    Identity Info
                                </td>
                                <td>
                                    {IdentityData.remarks}
                                </td>
                                <td style={{ background: IdentityData.color, color: "white" }}>
                                    {IdentityData.color}
                                </td>
                            </tr>
                            <tr>
                                <td className='custom_td'>
                                    Criminal
                                </td>
                                <td>
                                    Criminal Verification
                                </td>
                                <td>
                                    {CriminalData.remarks}
                                </td>
                                <td style={{ background: CriminalData.color, color: "white" }}>
                                    {CriminalData.color}
                                </td>
                            </tr>
                            <tr>
                                <td rowSpan={2} className='custom_td'>
                                    Experience
                                </td>
                                <td>
                                    Previous Employment
                                </td>
                                <td>
                                    {PreviousEmploymentData.remarks}
                                </td>
                                <td style={{ background: PreviousEmploymentData.color, color: "white" }}>
                                    {PreviousEmploymentData.color}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Project Details
                                </td>
                                <td>
                                    {ProjectDetailsData.remarks}
                                </td>
                                <td style={{ background: ProjectDetailsData.color, color: "white" }}>
                                    {ProjectDetailsData.color}
                                </td>
                            </tr>

                        </tbody>
                    </Table>
                </div>
            </div>
        </div>
    )
}

export default SummeryTable