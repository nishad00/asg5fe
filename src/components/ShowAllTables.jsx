import React, { useState, useEffect } from 'react'
import OverallView from './OverallView'
import SummeryTable from './SummeryTable'
import HigherEducationForm from './forms/HigherEducationForm'
import IdentityForm from './forms/IdentityForm'
import CriminalForm from './forms/CriminalForm'
import PreviousEmploymentForm from './forms/PreviousEmploymentForm'
import ProjectDetailsForm from './forms/ProjectDetailsForm'


const ShowAllTables = () => {

  const [overallViewData, setOverallViewData] = useState({
    isIdPresent: "",
    userID: "",
    userName: ""
  })

  const [higherEduData, setHigherEduData] = useState({
    remarks: "",
    color: ""
  })

  const [IdentityData, setIdentityData] = useState({
    remarks: "",
    color: ""
  })

  const [CriminalData, setCriminalData] = useState({
    remarks: "",
    color: ""
  })

  const [PreviousEmploymentData, setPreviousEmploymentData] = useState({
    remarks: "",
    color: ""
  })

  const [ProjectDetailsData, setProjectDetailsData] = useState({
    remarks: "",
    color: ""
  })

  
  
  return (
    <div>
      <div className="container py-5"  >
        <OverallView setOverallViewData={setOverallViewData} />

        <SummeryTable higherEduData={higherEduData} IdentityData={IdentityData} CriminalData={CriminalData} PreviousEmploymentData={PreviousEmploymentData} ProjectDetailsData={ProjectDetailsData} />

        <HigherEducationForm overallViewData={overallViewData} setHigherEduData={setHigherEduData} />

        <IdentityForm overallViewData={overallViewData} setIdentityData={setIdentityData} />

        <CriminalForm overallViewData={overallViewData} setCriminalData={setCriminalData} />

        <PreviousEmploymentForm overallViewData={overallViewData} setPreviousEmploymentData={setPreviousEmploymentData} />

        <ProjectDetailsForm overallViewData={overallViewData} setProjectDetailsData={setProjectDetailsData} />

      </div>
    </div>
  )
}

export default ShowAllTables