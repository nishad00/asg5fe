import React from 'react'
import ShowAllTables from '../components/ShowAllTables'

// React-Toastify
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Home = () => {
  return (
    <div>
      <ToastContainer position="bottom-right" autoClose="5000" theme="colored" />
      <ShowAllTables />
    </div>
  )
}

export default Home